// FUNCIONES PARA AUTENTICACIÓN
import { onAuthStateChanged, signOut } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-auth.js"
import { autenticacion } from "./firebase.js"
// FUNCIONES PARA REALIZAR CONSULTAS
import {getDatabase,ref,set,child,get,update,remove, onValue }from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js"
import {getStorage, ref as refS, uploadBytes, getDownloadURL} from "https://www.gstatic.com/firebasejs/9.13.0/firebase-storage.js"
// INICIACIÓN
const db = getDatabase();



// --------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------
// DECLARACIONES
var divContenido = document.getElementById('contenido');



// --------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------
// Ejecuciones
mostrarProductos();



// --------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------
// FUNCIONES
function mostrarProductos(){
    const db = getDatabase();
    const dbRef = ref(db, 'productos');

    onValue(dbRef, (snapshot)=>{
        snapshot.forEach((childSnapshot) => {
            const childKey = childSnapshot.key;
            const childData = childSnapshot.val();

            if(childData.estado == 0){
                divContenido.innerHTML +=
                    "<div class='divProducto'>" +
                        "<div class='producto'>" +
                            "<picture><img src='" + childData.url + "' alt='Empanada_" + childData.nombre + "'></picture>" +
                            "<div>" +
                                "<h2>Empanada</h2>" +
                                "<h3>de</h3>" +
                                "<h2 id='lblSabor' class='lblSabor'>" + childData.nombre + "</h2>" +
                            "</div>" +
                        "</div>" +
                        "<div>" +
                            "<hr>" +
                            "<h3 id='' class='lblPrecio'>$" + childData.precio + "</h3>" +
                            "<hr>" +
                        "</div>" +
                        "<div class='descripcion'>" +
                            "<p>" + childData.descripcion + "</p>" +
                        "</div>" +
                    "</div>";
            }
        });
    },{
        onlyOnce: true
    });
}