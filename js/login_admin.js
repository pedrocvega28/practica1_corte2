import { signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-auth.js"
import { autenticacion } from "./firebase.js"

const formularioLogin = document.getElementById('formulario');

formularioLogin.addEventListener('submit', async e =>{
    e.preventDefault();

    const correo = formularioLogin['inputCorreo'].value;
    const contraseña = formularioLogin['inputContraseña'].value;

    try{
        const credenciales = await signInWithEmailAndPassword(autenticacion, correo, contraseña);
        console.log(credenciales);

        location = '/html/administrador.html';
    }
    catch (error){
        alert("Correo y/o contraseña incorrectas");
    }
})